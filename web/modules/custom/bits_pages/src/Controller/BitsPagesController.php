<?php

namespace Drupal\bits_pages\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\Core\Url; use Drupal\Core\Link; 
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface; 
use Drupal\Core\Datetime;

/**
 * Returns responses for BITS Pages routes.
 */
class BitsPagesController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function hello() {
    return array('#markup' => '<p>' . $this->t('Pagina con mensaje simple') . '</p>',);
  }

  protected $currentUser, $dateFormatter; 
   
  public function __construct(AccountInterface $current_user) {
    $this->currentUser = $current_user;
    
    }
     
  public static function create(ContainerInterface $container) {
    return new static($container->get('current_user'));
    } 

 


 
  public function links() {
     
    //return array('#markup' => '<p>' . $this->t('links !!!!') . '</p>',);
   
    //link to /admin/structure/blocks

    // protected $fecha = date();
    
    $url1 = Url::fromRoute('block.admin_display');
    $link1 = Link::fromTextAndUrl(t('Administración de Bloques'), $url1);

    //link to /admin/people
    $url2 = Url::fromRoute('entity.user.collection');
    $link2 = Link::fromTextAndUrl(t('Administración de usuarios'), $url2);

    $url3 = Url::fromRoute('<front>');
    $link3 = Link::fromTextAndUrl(t('Enlace a la portada del sitio'), $url3); 

    $url4 = Url::fromUri('internal:/admin/content');
    $link4 = Link::fromTextAndUrl(t('Administración de Contenidos'), $url4); 

    $url5 = Url::fromRoute('entity.node.canonical', ['node' => 1]);
    $link5 = Link::fromTextAndUrl(t('Enlace al nodo con id 1'), $url5);

    $url6 = Url::fromRoute('entity.node.edit_form', ['node' => 1]);
    $link6 = Link::fromTextAndUrl(t('Enlace a la edición del nodo con id 1'), $url6); 

    $url7 = Url::fromUri('https://www.google.com');
    $link_options = array('attributes' => array(
                                          //'class' => array(
                                          //'external-link','list'),
                                          'target' => '_blank', 
                                          //'title' => 'Go to drupal.org',
                                        ),);
    $url7->setOptions($link_options);

    $link7 = Link::fromTextAndUrl(t('Enlace a www.google.com'), $url7);
 
       

    $list[] = $link1;
    $list[] = $link4;
    $list[] = $link2; 
    $list[] = $link3; 
    $list[] = $link5; 
    $list[] = $link6; 
    $list[] = $link7; 

    
    
    $output['bits_pages_links'] = array(
            '#theme' => 'item_list',
            '#items' => $list,
            '#title' => $this->t('Examples of links:')); 
            
    return $output;

  }


  public function calculator($num1, $num2) { 

    // return array('#markup' => '<p>' . $this->t('Comprobando que entra aqui') . '</p>',);

 
    //a) comprobamos que los valores facilitados sean numéricos
    //y si no es así, lanzamos una excepción
    if (!is_numeric($num1) || !is_numeric($num2)) {
      throw new BadRequestHttpException(t('No numeric arguments specified.'));
      } 
 
    //b) Los resultados se mostrarán en formato lista HTML (ul).
    //Cada elemento de la lista se añade a un array
    $list[] = $this->t("@num1 + @num2 = @sum",
                       array('@num1' => $num1,
                            '@num2' => $num2,
                            '@sum' => $num1 + $num2));
    $list[] = $this->t("@num1 - @num2 = @difference",
                       array('@num1' => $num1,
                             '@num2' => $num2,
                             '@difference' => $num1 - $num2));
    $list[] = $this->t("@num1 x @num2 = @product",
                       array('@num1' => $num1,
                              '@num2' => $num2,
                              '@product' => $num1 * $num2));
    //c) Evitar error de división por cero     
    if ($num2 != 0) 
        $list[] = $this->t("@num1 / @num2 = @division",
                           array('@num1' => $num1,
                                 '@num2' => $num2,
                                 '@division' => $num1 / $num2));
    else
         $list[] = $this->t("@num1 / @num2 = undefined (division by zero)",
                           array('@num1' => $num1,
                           '@num2' => $num2));

    //d) Se transforma el array $list en una lista HTML (ul)
   
    $output1 =''; 

    if($this->currentUser->hasPermission('administer nodes')){
      $output1 = $this->t('Este es un texto extra que solo se muestra cuando el usuario actual puede administrar nodos') . '</p>';
      } 

    $output['bits_pages_calculator'] = array(
          '#theme' => 'item_list',
          // '#markup' => $output1,
          '#items' => $list,
          '#title' => $this->t('Operations: <p> Este es un texto general </p>' . $output1),); 
 
    //e) Se devuelve el array renderizable con la salida.
    return $output;   
    } 
  } //cierre de class 
  

  

