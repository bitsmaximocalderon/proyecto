<?php

namespace Drupal\training_drupal_8\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "training_drupal_8_example",
 *   admin_label = @Translation("Example"),
 *   category = @Translation("Training Drupal 8")
 * )
 */
class ExampleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#markup' => $this->t('It works!'),
    ];
    return $build;
  }

}
