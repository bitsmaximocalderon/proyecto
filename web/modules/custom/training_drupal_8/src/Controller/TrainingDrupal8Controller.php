<?php

namespace Drupal\training_drupal_8\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Training Drupal 8 routes.
 */
class TrainingDrupal8Controller extends ControllerBase {

  /**
   * Builds the response.
   */
 
  public function hello() {
    return array('#markup' => '<p>' . $this->t('Welcome, Training Drupal 8! This is my first module in Drupal 8!') . '</p>',);
  }

  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
   } 

  

}
