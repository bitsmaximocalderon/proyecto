<?php

namespace Drupal\clima;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the clima entity type.
 */
class ClimaAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view clima');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit clima', 'administer clima'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete clima', 'administer clima'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create clima', 'administer clima'], 'OR');
  }

}
