<?php

namespace Drupal\clima;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a clima entity type.
 */
interface ClimaInterface extends ContentEntityInterface, EntityOwnerInterface {

}
