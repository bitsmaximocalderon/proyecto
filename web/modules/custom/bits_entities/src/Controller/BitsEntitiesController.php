<?php

namespace Drupal\bits_entities\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Bits Entities routes.
 */
class BitsEntitiesController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
