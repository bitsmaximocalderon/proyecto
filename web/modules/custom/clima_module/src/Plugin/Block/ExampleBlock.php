<?php

namespace Drupal\clima_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Component\Serialization\Json;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "clima_module_example",
 *   admin_label = @Translation("Example"),
 *   category = @Translation("Clima Module")
 * )
 */
class ExampleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */

 /* public function build() {
    /** @var \GuzzleHttp\Client $client */
  /*   $client = \Drupal::service('http_client_factory')->fromOptions([
      'base_uri' => 'https://cat-fact.herokuapp.com/',
    ]);

    $response = $client->get('facts/random', ['query' => ['amount' => 2,]]);
    //$response = $client->get('facts/random');

    $cat_facts = Json::decode($response->getBody());
    $items = [];

    foreach ($cat_facts as $cat_fact) {
      $items[] = $cat_fact['text'];
    }

    return [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
  }
 */
  public function build() {
    /** @var \GuzzleHttp\Client $client */

    $client = \Drupal::service('http_client_factory')->fromOptions([
      'base_uri' => 'http://api.openweathermap.org/data/2.5/weather?q=LONDON&appid=b1b15e88fa797225412429c1c50c122a1', ]);

    // $response = $client->get();
   
    // $cat_facts = Json::decode($client->getBody());

    $build['content'] = [
      '#markup' => $this->t('It works!'),
    ];
    return $build;
  }


}
