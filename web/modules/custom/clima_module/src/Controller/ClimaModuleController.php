<?php

namespace Drupal\clima_module\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Serialization\Json;

/**
 * Returns responses for Clima Module routes.
 */
class ClimaModuleController extends ControllerBase {

  /**
   * Builds the response.
   */

  public function ciudad() {
    /** @var \GuzzleHttp\Client $client */
    $client = \Drupal::service('http_client_factory')->fromOptions([
      'base_uri' => 'http://api.openweathermap.org/data/2.5/weather',
    ]);

    $CITY_NAME = 'MIAMI';
    $COUNTRY_COD = 'US';
    $appid = 'f7f726dac4af70b5f506886e2f8c7c70';
    $ubicacion = $CITY_NAME . ',' . $COUNTRY_COD;


    $response = $client->get('', ['query' => ['q' => $ubicacion, 'appid' => $appid]]);

    $cat_facts = Json::decode($response->getBody());
    //print_r($var1);
    
    /* $var = $cat_facts[main][temp];
    print_r($var);

    $var = $cat_facts[main][temp_min];
    print_r($var);
    $var = $cat_facts[main][temp_max];
    print_r($var); */
    
    $items[] = 'Temperatura' . '   ' . $cat_facts[main][temp];
    $items[] = 'Temp. MINIMA' . ' ->  ' . $cat_facts[main][temp_min];
    $items[] = 'Temp. MAXIMA' . ' ->  ' . $cat_facts[main][temp_max];
    $items[] = 'Presión'  . ' ->  ' . $cat_facts[main][pressure];
    $items[] = 'Humedad'  . ' ->  ' . $cat_facts[main][humidity];
    
    /* foreach ($cat_facts as $cat_fact) {
      $items[] = $cat_fact['weather']['id'];
    } */

    
      $output['content'] = array(
        '#theme' => 'item_list',
        '#items' => $items,
       );

      return $output;
  
  }

  public function cats() {
    /** @var \GuzzleHttp\Client $client */
   $client = \Drupal::service('http_client_factory')->fromOptions([
      'base_uri' => 'https://cat-fact.herokuapp.com/facts/random',
    ]);

    $response = $client->get('', ['query' => ['amount' => 2,]]);

    $cat_facts = Json::decode($response->getBody());
    $nodo = '';
    $items = [];

    foreach ($cat_facts as $cat_fact) {
      $nodo = $cat_fact['_id'] . '-->' . $cat_fact['text'];
      $items[] = $nodo;
      

    }

    $bulid['content'] = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];

    return $bulid;
  }

  /* public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  } */
}

  


