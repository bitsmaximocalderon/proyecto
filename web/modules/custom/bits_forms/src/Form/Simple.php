<?php

namespace Drupal\bits_forms\Form;
 
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;
use Egulias\EmailValidator\EmailValidator;


/**
 * Implements the Simple form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */

class Simple extends FormBase {
    
    protected $database;
    protected $currentUser;
    protected $emailValidator;
 
    public function __construct(Connection $database,
                                AccountInterface $current_user,
                                EmailValidator $email_validator) {
        $this->database = $database;
        $this->currentUser = $current_user;
        $this->emailValidator = $email_validator;
       } 
   
    public static function create(ContainerInterface $container) {
        return new static(
                $container->get('database'), 
                $container->get('current_user'),
                $container->get('email.validator'));
           }
 
    public function buildForm(array $form, FormStateInterface $form_state) { 

        $form['titulo'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Titulo'),
            '#description' => $this->t('The title must be at least 5 characters long.'),
            '#required' => TRUE,     
        ];

        $form['nombre'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Nombre de usuario'),
            '#description' => $this->t('The name must be at least 5 characters long.'),
            '#required' => FALSE,     
        ];

        $form['email'] = [
            '#type' => 'email',
            '#title' => $this->t('Correo Electrónico'),
            '#description' => $this->t('Correo electrónico del usuario'),
            '#required' => FALSE,     
        ];

        $form['username'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Username'),
            '#description' => $this->t('Your username.'),
            '#default_value' => $this->currentUser->getAccountName(),
            '#required' => TRUE,
            ];

        $form['currentId'] = [
            '#type' => 'int',
            '#default_value' => $this->currentUser->id(),
            '#required' => TRUE,
            ];

        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Submit'),
        ];

 
        return $form;  
    } 
 
    public function getFormId() {     return 'bits_forms_simple';   } 
 
    public function validateForm(array &$form, FormStateInterface $form_state) {
        $title = $form_state->getValue('titulo');
        $email = $form_state->getValue('email'); 
        $inicio = substr($title, 0, 1);
        $user = $this->currentUser->id();
           
        if (strlen($title) < 5 || strlen($title) >= 30 || $inicio < "A" || $inicio > "Z")
             {
             // Set an error for the form element with a key of "title".
             $form_state->setErrorByName('titulo', $this->t('El Titulo debe tener entre 5 y 30 caracteres y debe comenzar en mayuscula'));
             }

        if(!$this->emailValidator->isValid($email)) 
            {
            $form_state->setErrorByName('email', 
                         $this->t('%email Debe ingresar una direccion de correo válida',
                         ['%email' => $email]));  
             }
        
        if ($user == 0)
            {
            $form_state->setErrorByName('curentId', $this->t('Usuario no permitido'));

            }

        
     } 
 
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $title = $form_state->getValue('titulo');
        $username = $form_state->getValue('nombre');
        $user_email = $form_state->getValue('email'); 

        $this->database->insert('bits_forms_simple')
             ->fields(['titulo' => $form_state->getValue('titulo'),
                       'nombre' => $form_state->getValue('nombre'),
                       'email' => $form_state->getValue('email'),
                       'uid' => $this->currentUser->id(),
                       'ip' => \Drupal::request()->getClientIP(),
                       'timestamp' => REQUEST_TIME,
                       ])
             ->execute();

             drupal_set_message($this->t('The form has been submitted correctly')); 
 
             \Drupal::logger('bits_forms')->notice('New Simple Form entry from user %uid %nombre inserted: %titulo.',
                  [
                      '%nombre' => $form_state->getValue('nombre'),
                      '%titulo' => $form_state->getValue('titulo'), 
                      '%uid'=> $this->currentUser->id(),
                    ]);

            $form_state->setRedirect('bits_pages.hello');


        
    } 
} 