<?php

namespace Drupal\training_d8_console\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "training_d8_console_example",
 *   admin_label = @Translation("Example"),
 *   category = @Translation("Training Drupal 8 Console")
 * )
 */
class ExampleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#markup' => $this->t('It works!'),
    ];
    return $build;
  }

}
