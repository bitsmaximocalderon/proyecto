<?php

namespace Drupal\training_d8_console\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Training Drupal 8 Console routes.
 */
class TrainingD8ConsoleController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
