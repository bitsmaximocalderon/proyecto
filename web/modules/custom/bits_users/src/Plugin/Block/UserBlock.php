<?php

namespace Drupal\bits_users\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface; 
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult; 

/**
 * Provides an userblock block.
 *
 * @Block(
 *   id = "bits_users_userblock",
 *   admin_label = @Translation("UserBlock"),
 *   category = @Translation("BITS")
 * )
 */
class UserBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected $database;
  protected $currentUser;

  public function __construct(array $configuration,
                                  $plugin_id,
                                  $plugin_definition,
                                  AccountInterface $current_user,
                                  Connection $database) { 
    parent::__construct($configuration, $plugin_id, $plugin_definition); 
    $this->currentUser = $current_user;
    $this->database = $database;
   }
   
   public static function create(ContainerInterface $container,
                                    array $configuration, 
                                    $plugin_id, $plugin_definition) {
        return new static(
                $configuration,
                $plugin_id,
                $plugin_definition,
                $container->get('current_user'),
                $container->get('database'));
            }

  /**
   * {@inheritdoc}
   */
  public function build() {
    
    $list = [];
    $list[] = $this->currentUser->id();
    $list[] = $this->currentUser->getDisplayName();
    $list[] = $this->currentUser->getRoles();
    $list[] = $this->currentUser->getEmail();
    $list[] = date('j/m/Y  H:i:s', $this->currentUser->getLastAccessedTime());


    $build[] = [
      '#theme' => 'item_list',
      '#items' => $list,
      '#title' => $this->t('INFORMACION DE USUARIO ACTUAL'),
    ];
    return $build;
  }




   /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label' => 'TITULO PRINCIPAL',
      'label_display' => FALSE,
      'foo' => $this->t('Hello world!'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    
    $form['foo'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Foo'),
      '#default_value' => $this->configuration['foo'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['foo'] = $form_state->getValue('foo');
  }

  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access user block');
    }

}
