<?php

namespace Drupal\bits_entities_message\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Message type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "message_type",
 *   label = @Translation("Message type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\bits_entities_message\Form\MessageTypeForm",
 *       "edit" = "Drupal\bits_entities_message\Form\MessageTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\bits_entities_message\MessageTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer message types",
 *   bundle_of = "message",
 *   config_prefix = "message_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/message_types/add",
 *     "edit-form" = "/admin/structure/message_types/manage/{message_type}",
 *     "delete-form" = "/admin/structure/message_types/manage/{message_type}/delete",
 *     "collection" = "/admin/structure/message_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class MessageType extends ConfigEntityBundleBase {

  /**
   * The machine name of this message type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the message type.
   *
   * @var string
   */
  protected $label;

}
