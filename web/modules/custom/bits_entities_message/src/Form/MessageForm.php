<?php

namespace Drupal\bits_entities_message\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the message entity edit forms.
 */
class MessageForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New message %label has been created.', $message_arguments));
      $this->logger('bits_entities_message')->notice('Created new message %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The message %label has been updated.', $message_arguments));
      $this->logger('bits_entities_message')->notice('Updated new message %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.message.canonical', ['message' => $entity->id()]);
  }

}
