<?php

namespace Drupal\bits_entities_message;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/* *
 * Provides an interface defining a message entity type.
 * 
 * @ingroup bits_entities
 */
interface MessageInterface extends ContentEntityInterface, EntityOwnerInterface {




}
