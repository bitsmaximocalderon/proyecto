<?php

namespace Drupal\listado;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a listado entity type.
 */
interface ListadoInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the listado title.
   *
   * @return string
   *   Title of the listado.
   */
  public function getTitle();

  /**
   * Sets the listado title.
   *
   * @param string $title
   *   The listado title.
   *
   * @return \Drupal\listado\ListadoInterface
   *   The called listado entity.
   */
  public function setTitle($title);

  /**
   * Gets the listado creation timestamp.
   *
   * @return int
   *   Creation timestamp of the listado.
   */
  public function getCreatedTime();

  /**
   * Sets the listado creation timestamp.
   *
   * @param int $timestamp
   *   The listado creation timestamp.
   *
   * @return \Drupal\listado\ListadoInterface
   *   The called listado entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the listado status.
   *
   * @return bool
   *   TRUE if the listado is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the listado status.
   *
   * @param bool $status
   *   TRUE to enable this listado, FALSE to disable.
   *
   * @return \Drupal\listado\ListadoInterface
   *   The called listado entity.
   */
  public function setStatus($status);

}
