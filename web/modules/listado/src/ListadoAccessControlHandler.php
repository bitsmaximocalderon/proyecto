<?php

namespace Drupal\listado;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the listado entity type.
 */
class ListadoAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view listado');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit listado', 'administer listado'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete listado', 'administer listado'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create listado', 'administer listado'], 'OR');
  }

}
