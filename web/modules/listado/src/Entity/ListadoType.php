<?php

namespace Drupal\listado\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Listado type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "listado_type",
 *   label = @Translation("Listado type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\listado\Form\ListadoTypeForm",
 *       "edit" = "Drupal\listado\Form\ListadoTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\listado\ListadoTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer listado types",
 *   bundle_of = "listado",
 *   config_prefix = "listado_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/listado_types/add",
 *     "edit-form" = "/admin/structure/listado_types/manage/{listado_type}",
 *     "delete-form" = "/admin/structure/listado_types/manage/{listado_type}/delete",
 *     "collection" = "/admin/structure/listado_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class ListadoType extends ConfigEntityBundleBase {

  /**
   * The machine name of this listado type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the listado type.
   *
   * @var string
   */
  protected $label;

}
